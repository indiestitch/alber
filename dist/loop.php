<?php if (have_posts()): while (have_posts()) : the_post(); ?>


	<?php 
		$post_classes = false;
		$has_post_thumbnail = false;
	?>

	<?php if ( has_post_thumbnail()) : // Check if thumbnail exists 
		$has_post_thumbnail = true;
		$post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
		$imgmeta = wp_get_attachment_metadata( $post_thumbnail_id );
			if ($imgmeta['width'] > $imgmeta['height']) {
				$post_classes[] = "post-image-horizontal";
			} elseif ($imgmeta['width'] < $imgmeta['height']) {
				$post_classes[] = "post-image-vertical";
			} else {
				$post_classes[] = "post-image-square";
			}
		else : 
			$post_classes[] = 'no-post-thumbnail';
		endif;


	?>

	<!-- article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class($post_classes); ?>>

		
		<?php if ( $has_post_thumbnail ) : // Check if thumbnail exists ?>
			<!-- post thumbnail -->
			<div class="post-image">
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
					<?php the_post_thumbnail(); // Declare pixel size you need inside the array ?>
				</a>
			</div>
			<!-- /post thumbnail -->
		<?php endif; ?>

		<div class="post-content">
			<div class="post-cover">
				<div class="post-category"><?php the_category(', '); // Separated by commas ?></div>
				<!-- post title -->
				<h2 class="post-title">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
				</h2>
				<!-- /post title -->

				<!-- post details -->
				<div class="post-meta">
				    <div class="post-author"><?php echo get_avatar( get_the_author_meta( 'ID' ), 64 ); ?></div>
					<div class="post-date">
						<time datetime="<?php the_time('Y-m-d'); ?> <?php the_time('H:i'); ?>">
							<?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?>
						</time>
					</div>
					<div class="post-author"> <?php _e( 'By', 'html5blank' ); ?> <?php the_author_posts_link(); ?> </div>
				</div>
				<!-- /post details -->

			</div><!-- /post-cover -->

			<div class="post-lede">
				<?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>
			</div> 

			<?php edit_post_link(); ?> 
		</div>

	</article>
	<!-- /article -->

<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>
