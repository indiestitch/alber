<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="site-main">
			<div class="main-content">

			<h1><?php echo sprintf( __( '%s Search Results for ', 'html5blank' ), $wp_query->found_posts ); echo get_search_query(); ?></h1>

			<div class="content-posts">
				<?php get_template_part('loop'); ?>
			</div>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
