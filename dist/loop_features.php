


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="post-image">
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
			<?php the_post_thumbnail(); // Declare pixel size you need inside the array ?>
		</a>
	</div>

	<div class="post-content">
		<div class="post-category">
			<?php the_category(', '); // Separated by commas ?>
		</div>
		<!-- post title -->
		<h2 class="post-title">
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
		</h2>
		<!-- /post title -->
		<div class="post-meta">
			<div class="post-author"> <?php _e( 'By', 'html5blank' ); ?> <?php the_author_posts_link(); ?> </div>
		</div>
	</div>
</article>