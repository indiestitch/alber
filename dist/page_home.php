<?php
/*
Template Name: Home Page
*/
?>
<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="site-main">

			<div class="main-billboard">
                <div class="wrapper">
               	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
               		<?php the_content(); ?>
                <?php endwhile; ?>
            	<?php endif; ?>
                </div>
            </div>


<?php // Find Category IDs for future posts
	$cover_obj = get_category_by_slug('cover'); 
	$cover_id = $cover_obj->term_id;

	$feature_obj = get_category_by_slug('feature'); 
	$feature_id = $feature_obj->term_id;
?>
			<div class="main-content">
				<section class="content-front-page">
                    <div class="front-page-cover">

                    	<?php
	                    	$args = array(
							    'post_type'     => 'post',
							    'post_status'   => 'publish',
							    'cat' => $cover_id,
							    'posts_per_page' => '1'
							);
							$cover_query = new WP_Query( $args );
							if( $cover_query->have_posts() ): while ($cover_query->have_posts()) : $cover_query->the_post(); ?>

							<?php get_template_part('loop_home'); ?>


							<?php endwhile; ?>
							<?php endif; ?>
							<?php wp_reset_query(); ?>

                    </div> <!-- /front-page-cover -->

                    <div class="front-page-features">


						<?php
							
	                    	$args = array(
							    'post_type'     => 'post',
							    'post_status'   => 'publish',
							    'cat' => $feature_id,
							    'posts_per_page' => '4',
							    'category__not_in' => $cover_id
							);
							$cover_query = new WP_Query( $args );
							if( $cover_query->have_posts() ): while ($cover_query->have_posts()) : $cover_query->the_post(); ?>

							<?php get_template_part('loop_features'); ?>


							<?php endwhile; ?>
							<?php endif; ?>
							<?php wp_reset_query(); ?>

                    </div>
                </section> <!-- /content-front-page -->


                <section class="content-latest">

                	<?php
							
	                	$args = array(
						    'post_type'     => 'post',
						    'post_status'   => 'publish',
						    'posts_per_page' => '8',
						    'category__not_in' => array($cover_id,$feature_id)
						);
						$cover_query = new WP_Query( $args );
						if( $cover_query->have_posts() ): while ($cover_query->have_posts()) : $cover_query->the_post(); ?>

						<?php get_template_part('loop_features'); ?>


						<?php endwhile; ?>
						<?php endif; ?>
						<?php wp_reset_query(); ?>

                </section> <!-- /content-latest -->



			</div>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>





<div class="site-main">




                <section class="sponsor sponsor-primary">
                    <div class="sponsor-content">
                        <div class="sponsor-title">SHOP HARD</div>
                        <div class="sponsor-subtitle">This is how we do</div>
                        <div class="sponsor-cta">Shop Now</div>
                    </div>
                </section>


                <section class="content-latest" >

                    

                    <div class="post grid-sizer">
                        <div class="post-image">
                            <a href="#"><img src="http://placekitten.com/1200/1600"></a>
                        </div>

                        <div class="post-content">

                            <a class="post-category" href="#">Runway Review</a>

                            <h3 class="post-title"><a href="#">The  Art Museum  the major  archive “Emphatics”</a></h3>

                            <div class="post-meta">
                                <div class="post-author">by <a href="#">Madison Samantha</a></div>
                            </div>
                        </div>
                    </div>


                    <div class="post">
                        <div class="post-image">
                            <a href="#"><img src="http://placekitten.com/800/450"></a>
                        </div>

                        <div class="post-content">

                            <a class="post-category" href="#">Runway Review</a>

                            <h3 class="post-title"><a href="#">The Phoenix Art Museum acquires the major fashion archive “Emphatics”</a></h3>

                            <div class="post-meta">
                                <div class="post-author">by <a href="#">Madison Samantha</a></div>
                            </div>
                        </div>
                    </div>


                    <div class="post">
                        <div class="post-image">
                            <a href="#"><img src="http://placekitten.com/800/450"></a>
                        </div>

                        <div class="post-content">

                            <a class="post-category" href="#">Runway Review</a>

                            <h3 class="post-title"><a href="#">The Phoenix Art  acquires the major fashion archive “Emphatics”</a></h3>

                            <div class="post-meta">
                                <div class="post-author">by <a href="#">Madison Samantha</a></div>
                            </div>
                        </div>
                    </div>
                    

                    <div class="post feature">
                        <div class="post-image">
                            <a href="#"><img src="http://placekitten.com/1200/1600"></a>
                        </div>

                        <div class="post-content">

                            <a class="post-category" href="#">Runway Review</a>

                            <h3 class="post-title"><a href="#">The Phoemajor fashion archive “Emphatics”</a></h3>

                            <div class="post-meta">
                                <div class="post-author">by <a href="#">Madison Samantha</a></div>
                            </div>
                        </div>
                    </div>


                    <div class="post">
                        <div class="post-image">
                            <a href="#"><img src="http://placekitten.com/800/450"></a>
                        </div>

                        <div class="post-content">

                            <a class="post-category" href="#">Runway Review</a>

                            <h3 class="post-title"><a href="#">The Phoenix Art Museum acquires major fashion archive “Emphatics”</a></h3>

                            <div class="post-meta">
                                <div class="post-author">by <a href="#">Madison Samantha</a></div>
                            </div>
                        </div>
                    </div>


                    <div class="post">
                        <div class="post-image">
                            <a href="#"><img src="http://placekitten.com/800/450"></a>
                        </div>

                        <div class="post-content">

                            <a class="post-category" href="#">Runway Review</a>

                            <h3 class="post-title"><a href="#">The Pt Museum acquires the major fashion archive “Emphatics”</a></h3>

                            <div class="post-meta">
                                <div class="post-author">by <a href="#">Madison Samantha</a></div>
                            </div>
                        </div>
                    </div>





                    <div class="post">
                        <div class="post-image">
                            <a href="#"><img src="http://placekitten.com/1200/1600"></a>
                        </div>

                        <div class="post-content">

                            <a class="post-category" href="#">Runway Review</a>

                            <h3 class="post-title"><a href="#">The  Ar</a></h3>

                            <div class="post-meta">
                                <div class="post-author">by <a href="#">Madison Samantha</a></div>
                            </div>
                        </div>
                    </div>

                    <div class="post">
                        <div class="post-image">
                            <a href="#"><img src="http://placekitten.com/1200/1600"></a>
                        </div>

                        <div class="post-content">

                            <a class="post-category" href="#">Runway Review</a>

                            <h3 class="post-title"><a href="#">The ix Art Museum acquires the major fashion archive “Emphatics”</a></h3>

                            <div class="post-meta">
                                <div class="post-author">by <a href="#">Madison Samantha</a></div>
                            </div>
                        </div>
                    </div>



                </section>

            <div class="content-secondary">

                <div class="feature-front-row">
                    <div class="feature-cover">
                        <h2 class="feature-title">Eco Fashion Week - Spring 2015</h2>
                    </div>
                    <div class="feature-content">
                        <div class="post">
                            <div class="post-author"><a href="#">Lindsay Viker</a> wrote</div>
                            <h4 class="post-title"><a href="#">Obakki, H&M and Value Village Expected to Dominate Vancouver’s Eco Fashion Week</a></h4>
                        </div>
                        <div class="post">
                            <div class="post-author"><a href="#">Lindsay Viker</a> wrote</div>
                            <h4 class="post-title"><a href="#">Obakki, H&M and Value Village Expected to Dominate Vancouver’s Eco Fashion Week</a></h4>
                        </div>
                        <div class="post">
                            <div class="post-author"><a href="#">Lindsay Viker</a> wrote</div>
                            <h4 class="post-title"><a href="#">Obakki, H&M and Value Village Expected to Dominate Vancouver’s Eco Fashion Week</a></h4>
                        </div>
                        <div class="post">
                            <div class="post-author"><a href="#">Lindsay Viker</a> wrote</div>
                            <h4 class="post-title"><a href="#">Obakki, H&M and Value Village Expected to Dominate Vancouver’s Eco Fashion Week</a></h4>
                        </div>
                        <a class="feature-cta" href="#">Read More</a>
                    </div>
                </div>

            </div>




            <div class="pagination">
                <span class="page-numbers current">1</span>
            <a class="page-numbers" href="http://indiestitch.com/page/2/">2</a>
            <a class="page-numbers" href="http://indiestitch.com/page/3/">3</a>
            <a class="next page-numbers" href="http://indiestitch.com/page/2/">Next »</a></div>


        </div>


        </div>