<?php
/*
Template Name: Home Page
*/
?>
<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="site-main">

			<div class="main-billboard">
                <div class="wrapper">
               	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
               		<?php the_content(); ?>
                <?php endwhile; ?>
            	<?php endif; ?>
                </div>
            </div>


<?php // Find Category IDs for future posts
	$cover_obj = get_category_by_slug('cover'); 
	$cover_id = $cover_obj->term_id;

	$feature_obj = get_category_by_slug('feature'); 
	$feature_id = $feature_obj->term_id;
?>
			<div class="main-content">
				<section class="content-front-page">
                    <div class="front-page-cover">

                    	<?php
	                    	$args = array(
							    'post_type'     => 'post',
							    'post_status'   => 'publish',
							    'cat' => $cover_id,
							    'posts_per_page' => '1'
							);
							$cover_query = new WP_Query( $args );
							if( $cover_query->have_posts() ): while ($cover_query->have_posts()) : $cover_query->the_post(); ?>

							<?php get_template_part('loop_home'); ?>


							<?php endwhile; ?>
							<?php endif; ?>
							<?php wp_reset_query(); ?>

                    </div> <!-- /front-page-cover -->

                    <div class="front-page-features">


						<?php
							
	                    	$args = array(
							    'post_type'     => 'post',
							    'post_status'   => 'publish',
							    'cat' => $feature_id,
							    'posts_per_page' => '4',
							    'category__not_in' => $cover_id
							);
							$cover_query = new WP_Query( $args );
							if( $cover_query->have_posts() ): while ($cover_query->have_posts()) : $cover_query->the_post(); ?>

							<?php get_template_part('loop_features'); ?>


							<?php endwhile; ?>
							<?php endif; ?>
							<?php wp_reset_query(); ?>

                    </div>
                </section> <!-- /content-front-page -->


                <section class="content-latest">

                	<?php
							
	                	$args = array(
						    'post_type'     => 'post',
						    'post_status'   => 'publish',
						    'posts_per_page' => '8',
						    'category__not_in' => array($cover_id,$feature_id)
						);
						$cover_query = new WP_Query( $args );
						if( $cover_query->have_posts() ): while ($cover_query->have_posts()) : $cover_query->the_post(); ?>

						<?php get_template_part('loop_features'); ?>


						<?php endwhile; ?>
						<?php endif; ?>
						<?php wp_reset_query(); ?>

                </section> <!-- /content-latest -->



			</div>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>





<div class="site-main">




            <div class="content-secondary">

                <div class="feature-front-row">
                    <div class="feature-cover">
                        <h2 class="feature-title">Eco Fashion Week - Spring 2015</h2>
                    </div>
                    <div class="feature-content">
                        <div class="post">
                            <div class="post-author"><a href="#">Lindsay Viker</a> wrote</div>
                            <h4 class="post-title"><a href="#">Obakki, H&M and Value Village Expected to Dominate Vancouver’s Eco Fashion Week</a></h4>
                        </div>
                        <div class="post">
                            <div class="post-author"><a href="#">Lindsay Viker</a> wrote</div>
                            <h4 class="post-title"><a href="#">Obakki, H&M and Value Village Expected to Dominate Vancouver’s Eco Fashion Week</a></h4>
                        </div>
                        <div class="post">
                            <div class="post-author"><a href="#">Lindsay Viker</a> wrote</div>
                            <h4 class="post-title"><a href="#">Obakki, H&M and Value Village Expected to Dominate Vancouver’s Eco Fashion Week</a></h4>
                        </div>
                        <div class="post">
                            <div class="post-author"><a href="#">Lindsay Viker</a> wrote</div>
                            <h4 class="post-title"><a href="#">Obakki, H&M and Value Village Expected to Dominate Vancouver’s Eco Fashion Week</a></h4>
                        </div>
                        <a class="feature-cta" href="#">Read More</a>
                    </div>
                </div>

            </div>

        </div>


        </div>