<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="site-main">
			<div class="main-content">
			<h1><?php _e( 'Category: ', 'html5blank' ); single_cat_title(); ?></h1>

				<div class="content-posts">
					<?php get_template_part('loop'); ?>
				</div>

				<?php get_template_part('pagination'); ?>
			</div>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
