<?php get_header(); ?>

	<main role="main">
	<div class="main-content">
		<!-- section -->
		<section class="content-post">

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<!-- post thumbnail  // Hidden for a hot moment
				<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
						<?php the_post_thumbnail(); // Fullsize image for the single post ?>
					</a>
				<?php endif; ?>
	 /post thumbnail -->

				<section class="post-cover">
					<div class="post-category"><?php the_category(', '); // Separated by commas ?></div>
					<!-- post title -->
					<h1 class="post-title">
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"> <?php the_title(); ?></a>
					</h1>
					<!-- /post title -->
				</section>

				<section class="post-meta">

					<?php echo get_avatar( get_the_author_meta( 'ID' ) ); ?>

					<!-- post details -->
					<div class="post-date">
						<time datetime="<?php the_time('Y-m-d'); ?> <?php the_time('H:i'); ?>">
							<?php the_date(); ?>
						</time>
					</div>
					<div class="post-author"><?php _e( 'By', 'html5blank' ); ?> <?php the_author_posts_link(); ?></div>
					<!-- /post details -->
				</section>

				<section class="post-content">

					<?php the_content(); // Dynamic Content ?>

					<?php the_tags( __( 'Tags: ', 'html5blank' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>

					<p><?php _e( 'This post was written by ', 'html5blank' ); the_author(); ?></p>

					<?php edit_post_link(); // Always handy to have Edit Post Links available ?>

				</section>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
	</div>
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
